==================================================================
https://keybase.io/sitting33
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://33rd.dev
  * I am sitting33 (https://keybase.io/sitting33) on keybase.
  * I have a public key ASCCK5_oLG757IlQpKgh3pKgZ2MeQjf0VQz0IBFmVsVU9go

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01202d4cc9ae93aa81b9f279f54bec2acbeebdf9f660b7d89f7d4bda281d684c30490a",
      "host": "keybase.io",
      "kid": "0120822b9fe82c6ef9ec8950a4a821de92a067631e4237f4550cf420116656c554f60a",
      "uid": "a5ac99018abe3f1f8b0938326e333419",
      "username": "sitting33"
    },
    "merkle_root": {
      "ctime": 1635523014,
      "hash": "a5e759b2f7e162a7ceac54fd8ff37d6b92a9e692f9d2c50a5df64df754a01597467ec85cc42680a4a3b79fcf0718f7bbb0f029b8d4cc2d15f5f18901a2de3e1b",
      "hash_meta": "d7087792bac0f2da84003a66d344d538f8e6ddf78395582964a0e95791a589b9",
      "seqno": 21092157
    },
    "service": {
      "entropy": "rWI+2WXFsx2lHtZrKOKe+n9o",
      "hostname": "33rd.dev",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.8.1"
  },
  "ctime": 1635523060,
  "expire_in": 504576000,
  "prev": "7b8eb87d5cecd52081a14b7ae56dcb686a38e9d415dac0a9869be03424dafe9e",
  "seqno": 43,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEggiuf6Cxu+eyJUKSoId6SoGdjHkI39FUM9CARZlbFVPYKp3BheWxvYWTESpcCK8Qge464fVzs1SCBoUt65W3LaGo46dQV2sCphpvgNCTa/p7EIJDbe7DSKA5ylsbjCOGsC/trJohUn+bQ1k8X4iE+znj+AgHCo3NpZ8RAnknrOJoKb91Qrk1swhyHKUowQ0qgVbqoPHnl92oWvkDqUcToKvvVuEdY3asyRRfX9wONUrQ3xFOl0qF3CTf4C6hzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIIjZcgw5AwYBnTauwwIShe20wuDAxeY9N3I2/dTNF2Wao3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/sitting33

==================================================================
